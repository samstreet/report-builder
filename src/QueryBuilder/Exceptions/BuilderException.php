<?php

namespace QueryBuilder\Models\Exceptions;

use Exception;
use Throwable;

/**
 * @author Sam Street
 */
class BuilderException extends Exception {
	
	public function __construct( $message = "", $code = 0, Throwable $previous = null ) {
		
		parent::__construct( $message, $code, $previous );
	}
	
}