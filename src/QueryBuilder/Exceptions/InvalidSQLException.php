<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models\Exceptions;

use Exception;
use Throwable;

class InvalidSQLException extends Exception {
	
	public function __construct( $message = "", $code = 0, Throwable $previous = null ) {
		
		parent::__construct( $message, $code, $previous );
	}
}