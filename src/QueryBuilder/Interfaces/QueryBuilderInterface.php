<?php

namespace QueryBuilder\Interfaces;

/**
 * @author Sam Street
 */

interface QueryBuilderInterface {
    
    /**
     * @param array $parts
     * @return mixed
     */
    public function build();
    
}