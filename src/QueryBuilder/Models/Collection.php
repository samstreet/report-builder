<?php

/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

use Iterator;
use ArrayAccess;

/**
 * Class Collection
 *
 * @package QueryBuilder\Models
 */
class Collection implements Iterator, ArrayAccess {
	
	/**
	 * @var array
	 */
	private $array_elements;
	
	/**
	 * Collection constructor.
	 *
	 * @param array $array_elements
	 */
	function __construct( $array_elements = [] ) {
		
		$this->array_elements = $array_elements;
	}
	
	/**
	 * @param      $arg1
	 * @param bool $arg2
	 *
	 * @return $this
	 */
	public function add( $arg1, $arg2 = false ) {
		
		if ( ! $arg2 ) {
			$this->array_elements[] = $arg1;
		} else {
			if ( ! array_key_exists( $arg1, $this->array_elements ) ) {
				$this->array_elements[ $arg1 ] = $arg2;
			}
		}
		$this->count();
		
		return $this;
	}
	
	/**
	 * @param $key
	 * @param $item
	 *
	 * @return mixed
	 */
	public function set( $key, $item ) {
		
		if ( isset( $key ) ) {
			$this->array_elements[ $key ] = $item;
		} else {
			$this->array_elements[] = $item;
		}
		$this->count();
		
		return $this->get( $key );
	}
	
	/**
	 * @param null $flags
	 *
	 * @return $this
	 */
	public function asort( $flags = null ) {
		
		asort( $this->array_elements, $flags );
		
		return $this;
	}
	
	/**
	 * @param null $flags
	 *
	 * @return $this
	 */
	public function ksort( $flags = null ) {
		
		ksort( $this->array_elements, $flags );
		
		return $this;
	}
	
	/**
	 * @param null $flags
	 *
	 * @return $this
	 */
	public function sort( $flags = null ) {
		
		sort( $this->array_elements, $flags );
		
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function count() {
		
		return count( $this->array_elements );
	}
	
	/**
	 * @param $key
	 *
	 * @return $this
	 */
	public function remove( $key ) {
		
		if ( array_key_exists( $key, $this->array_elements ) ) {
			unset( $this->array_elements[ $key ] );
			$this->count();
			
			return $this;
		}
	}
	
	/**
	 * @return mixed
	 */
	public function next() {
		
		return next( $this->array_elements );
	}
	
	/**
	 * @return bool
	 */
	public function hasNext() {
		
		$this->next();
		$v = $this->valid();
		$this->back();
		
		return $v;
	}
	
	/**
	 * @return mixed
	 */
	public function back() {
		
		return prev( $this->array_elements );
	}
	
	/**
	 * @return mixed
	 */
	public function rewind() {
		
		return reset( $this->array_elements );
	}
	
	/**
	 * @return mixed
	 */
	public function forward() {
		
		return end( $this->array_elements );
	}
	
	/**
	 * @return mixed
	 */
	public function current() {
		
		return current( $this->array_elements );
	}
	
	/**
	 * @return mixed
	 */
	public function currentKey() {
		
		return key( $this->array_elements );
	}
	
	/**
	 * @return mixed
	 */
	public function key() {
		
		return $this->currentKey();
	}
	
	/**
	 * @return bool
	 */
	public function valid() {
		
		if ( ! is_null( $this->key() ) ) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param $key
	 *
	 * @return mixed
	 */
	public function get( $key ) {
		
		return $this->array_elements[ $key ];
	}
	
	/**
	 * @param mixed $offset
	 *
	 * @return mixed
	 */
	public function offsetExists( $offset ) {
		
		return $this->exists( $offset );
	}
	
	/**
	 * @param mixed $offset
	 *
	 * @return mixed
	 */
	public function offsetGet( $offset ) {
		
		return $this->get( $offset );
	}
	
	/**
	 * @param mixed $offset
	 * @param mixed $value
	 *
	 * @return mixed
	 */
	public function offsetSet( $offset, $value ) {
		
		return $this->set( $offset, $value );
	}
	
	/**
	 * @param mixed $offset
	 *
	 * @return \QueryBuilder\Models\Collection
	 */
	public function offsetUnset( $offset ) {
		
		return $this->remove( $offset );
	}
	
	/**
	 * @return bool
	 */
	public function isEmpty() {
		
		if ( $this->count() < 1 ) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param $obj
	 *
	 * @return bool
	 */
	public function contains( $obj ) {
		
		foreach ( $this->array_elements as $element ) {
			if ( $element === $obj ) {
				$this->rewind();
				
				return true;
			}
		}
		$this->rewind();
		
		return false;
	}
	
	/**
	 * @param $obj
	 *
	 * @return int|null|string
	 */
	public function indexOf( $obj ) {
		
		foreach ( $this->array_elements as $k => $element ) {
			if ( $element === $obj ) {
				$this->rewind();
				
				return $k;
			}
		}
		$this->rewind();
		
		return null;
	}
	
	/**
	 * @param $size
	 *
	 * @return $this
	 */
	public function trimToSize( $size ) {
		
		$t                    = array_chunk( $this->array_elements, $size, true );
		$this->array_elements = $t[ 0 ];
		$this->count();
		
		return $this;
	}
	
	/**
	 * @param callable $callback
	 */
	public function each( callable $callback ) {
		
		foreach ( $this->array_elements as $key => $value ) {
			$callback( $this->array_elements[ $key ] );
		}
		
	}
	
}
