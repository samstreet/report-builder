<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

/**
 * Class Config
 *
 * @package QueryBuilder\Models
 */
class Config {

	/**
	 * @var
	 */
	private $_main_table;

	/**
	 * @var
	 */
	private $_selectable;

	/**
	 * @var
	 */
	private $_joinable;

	/**
	 * @var
	 */
	private $_whereable;

	/**
	 * @var
	 */
	private $_sortable;

	/**
	 * @return mixed
	 */
	public function getMainTable() {
		return $this->_main_table;
	}

	/**
	 * @param mixed $main_table
	 */
	public function setMainTable( $main_table ) {
		$this->_main_table = $main_table;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSelectable() {
		return $this->_selectable;
	}

	/**
	 * @param mixed $selectable
	 */
	public function setSelectable( $selectable ) {
		$this->_selectable = $selectable;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getJoinable() {
		return $this->_joinable;
	}

	/**
	 * @param mixed $joinable
	 */
	public function setJoinable( $joinable ) {
		$this->_joinable = $joinable;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getWhereable() {
		return $this->_whereable;
	}

	/**
	 * @param mixed $whereable
	 */
	public function setWhereable( $whereable ) {
		$this->_whereable = $whereable;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSortable() {
		return $this->_sortable;
	}

	/**
	 * @param mixed $sortable
	 */
	public function setSortable( $sortable ) {
		$this->_sortable = $sortable;
		return $this;
	}


}