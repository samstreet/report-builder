<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

use QueryBuilder\Interfaces\QueryBuilderInterface;
use QueryBuilder\Models\Parts\From;

class FromQuery implements QueryBuilderInterface
{
    /**
     * @var string
     */
    private $selectPart;
    
    /**
     * @var From
     */
    private $part;
    
    /**
     * SelectQuery constructor.
     * @param From $from
     */
    function __construct(From $from)
    {
        $this->part = $from;
    }
    
    /**
     * @param array $parts
     * @return string
     */
    public function build()
    {
        return "FROM " . $this->part->getFrom() . ' ' . $this->part->getAlias() . ($this->part->getIndexBy() ? ' INDEX BY ' . $this->part->getIndexBy() : '');
    }
    
}
