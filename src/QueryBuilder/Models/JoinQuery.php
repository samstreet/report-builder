<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

use QueryBuilder\Interfaces\QueryBuilderInterface;
use QueryBuilder\Models\Parts\Join;

class JoinQuery implements QueryBuilderInterface
{
    /**
     * @var Join
     */
    private $join;
    
    function __construct(Join $join)
    {
        $this->join = $join;
    }
    
    /**
     * @return string
     */
    public function build()
    {
        return strtoupper($this->join->getJoinType()) . ' JOIN ' . $this->join->getJoin()
            . ($this->join->getAlias() ? ' ' . $this->join->getAlias() : '')
            . ($this->join->getIndexBy() ? ' INDEX BY ' . $this->join->getIndexBy() : '')
            . ($this->join->getCondition() ? ' ' . strtoupper($this->join->getConditionType()) . ' ' . $this->join->getCondition() : '')
            . ' ';
    }
    
}
