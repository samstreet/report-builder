<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

use QueryBuilder\Interfaces\JoinInterface;

class InnerJoin extends Join implements JoinInterface {
	
}