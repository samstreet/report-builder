<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

use QueryBuilder\Interfaces\JoinInterface;

class LeftJoin extends Join implements JoinInterface {
	
}