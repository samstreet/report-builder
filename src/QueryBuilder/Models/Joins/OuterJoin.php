<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

use QueryBuilder\Interfaces\JoinInterface;

class OuterJoin extends Join implements JoinInterface {
	
}