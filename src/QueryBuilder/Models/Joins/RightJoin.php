<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

use QueryBuilder\Interfaces\JoinInterface;

class RightJoin extends Join implements JoinInterface {
	
}