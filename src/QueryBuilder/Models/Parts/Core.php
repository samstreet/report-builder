<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models\Parts;


abstract class Core
{
    
    /**
     * @var string
     */
    protected $preChar = '(';
    
    /**
     * @var string
     */
    protected $postChar = ')';
    
    /**
     * @var string
     */
    protected $connector = ', ';
    
    /**
     * @var array
     */
    protected $parts = [];
    
    /**
     * Core constructor.
     *
     * @param $args
     */
    function __construct($args)
    {
        
        return $this->appendArgs($args);
    }
    
    /**
     * @param mixed[] $args
     *
     * @return Core
     */
    public function appendArgs($args = [])
    {
        
        foreach ((array)$args as $arg) {
            $this->add($arg);
        }
        
        return $this;
    }
    
    /**
     * @param mixed $arg
     *
     * @return Core
     *
     */
    public function add($arg)
    {
        
        $this->parts[] = $arg;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function count()
    {
        
        return count($this->parts);
    }
    
    /**
     * @return string
     */
    public function getPreChar()
    {
        return $this->preChar;
    }
    
    /**
     * @return string
     */
    public function getPostChar()
    {
        return $this->postChar;
    }
    
    /**
     * @return string
     */
    public function getConnector()
    {
        return $this->connector;
    }
    
    /**
     * @return array
     */
    public function getParts()
    {
        return $this->parts;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        
        if ($this->count() === 1) {
            return (string)$this->parts[0];
        }
        
        return $this->preChar . implode($this->connector, $this->parts) . $this->postChar;
    }
    
}