<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models\Parts;

/**
 * Class Select
 *
 * @package QueryBuilder\Models
 */
class Select extends Core
{
    
    /**
     * @var string
     */
    protected $preChar = '';
    
    /**
     * @var string
     */
    protected $postChar = '';
    
    /**
     * @return array
     */
    public function getParts()
    {
        return $this->parts;
    }
    
    
}