<?php

namespace QueryBuilder\Models;

use QueryBuilder\Interfaces\ReportInterface;
use QueryBuilder\Models\Parts\Select;
use QueryBuilder\Models\Parts\From;
use QueryBuilder\Models\Parts\Join;
use QueryBuilder\Traits\Formattable;

/**
 * Class QueryBuilder
 *
 * @package QueryBuilder
 */
class QueryBuilder implements ReportInterface
{
    
    use Formattable;
    
    /**
     * The parts of the array we
     * @var array
     */
    private $parts = [
        'distinct' => false,
        'select' => [],
        'from' => [],
        'join' => [],
        'set' => [],
        'where' => null,
        'groupBy' => [],
        'having' => null,
        'orderBy' => [],
    ];
    
    private $joinRootAliases = [];
    
    /**
     * QueryBuilder constructor.
     */
    function __construct()
    {
        $this->parts['select'] = new Collection();
        $this->parts['from'] = new Collection();
        $this->parts['join'] = new Collection();
        $this->parts['set'] = new Collection();
        $this->parts['where'] = new Collection();
        $this->parts['groupBy'] = new Collection();
        $this->parts['having'] = new Collection();
        $this->parts['orderBy'] = new Collection();
    }
    
    public function add($dqlPartName, $dqlPart, $append = false)
    {
        if ($append && ($dqlPartName === 'where' || $dqlPartName === 'having')) {
            throw new \InvalidArgumentException(
                "Using \$append = true does not have an effect with 'where' or 'having' " .
                'parts. See QueryBuilder#andWhere() for an example for correct usage.'
            );
        }
        $isMultiple = is_array($this->parts[$dqlPartName])
            && !($dqlPartName === 'join' && !$append);
        // Allow adding any part retrieved from self::getDQLParts().
        if (is_array($dqlPart) && $dqlPartName !== 'join') {
            $dqlPart = reset($dqlPart);
        }
        // This is introduced for backwards compatibility reasons.
        // TODO: Remove for 3.0
        if ($dqlPartName === 'join') {
            $newDqlPart = [];
            foreach ($dqlPart as $k => $v) {
                $k = is_numeric($k) ? $this->getRootAlias() : $k;
                $newDqlPart[$k] = $v;
            }
            $dqlPart = $newDqlPart;
        }
        if ($append && $isMultiple) {
            if (is_array($dqlPart)) {
                $key = key($dqlPart);
                $this->parts[$dqlPartName][$key][] = $dqlPart[$key];
            } else {
                $this->parts[$dqlPartName][] = $dqlPart;
            }
        } else {
            $this->parts[$dqlPartName] = ($isMultiple) ? [$dqlPart] : $dqlPart;
        }
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function make()
    {
        $select = (new SelectQuery($this->parts['select']))->build();
        $from = (new FromQuery($this->parts['from'][0]))->build();
        if (!empty($this->parts['join'])) {
            $join = " ";
            foreach ($this->parts['join'][$this->getRootAlias()] as $joiner) {
                $join .= (new JoinQuery($joiner))->build();
            }
        }
        return $select . " " . $from . " " . $join;
    }
    
    public function select($select = null)
    {
        
        if (empty($select)) {
            return $this;
        }
        
        $selects = is_array($select) ? $select : func_get_args();
        return $this->add('select', new Select($selects), false);
        
    }
    
    public function addSelect($select = null)
    {
    }
    
    public function delete($delete = null, $alias = null)
    {
    }
    
    public function update($update = null, $alias = null)
    {
    }
    
    public function set($key, $value)
    {
    }
    
    public function from($from, $alias, $indexBy = null)
    {
        return $this->add('from', new From($from, $alias, $indexBy), true);
    }
    
    public function join($join, $alias, $conditionType = null, $condition = null, $indexBy = null)
    {
    }
    
    public function innerJoin($join, $alias, $conditionType = null, $condition = null, $indexBy = null)
    {
        $hasParentAlias = strpos($join, '.');
        $parentAlias = substr($join, 0, $hasParentAlias === false ? 0 : $hasParentAlias);
        $rootAlias = $this->findRootAlias($alias, $parentAlias);
        $join = new Join(
            Join::INNER_JOIN,
            $join,
            $alias,
            $conditionType,
            $condition,
            $indexBy
        );
        return $this->add('join', [$rootAlias => $join], true);
    }
    
    public function leftJoin($join, $alias, $conditionType = null, $condition = null, $indexBy = null)
    {
        $hasParentAlias = strpos($join, '.');
        $parentAlias = substr($join, 0, $hasParentAlias === false ? 0 : $hasParentAlias);
        $rootAlias = $this->findRootAlias($alias, $parentAlias);
        $join = new Join(Join::LEFT_JOIN, $join, $alias, $conditionType, $condition, $indexBy);
        return $this->add('join', [$rootAlias => $join], true);
    }
    
    public function where($where)
    {
    }
    
    public function andWhere($where)
    {
    }
    
    public function orWhere($where)
    {
    }
    
    public function groupBy($groupBy)
    {
    }
    
    public function addGroupBy($groupBy)
    {
    }
    
    public function having($having)
    {
    }
    
    public function andHaving($having)
    {
    }
    
    public function orHaving($having)
    {
    }
    
    public function orderBy($sort, $order = null)
    {
    }
    
    public function addOrderBy($sort, $order = 'ASC')
    {
    }
    
    
    /**
     * Finds the root entity alias of the joined entity.
     *
     * @param string $alias The alias of the new join entity
     * @param string $parentAlias The parent entity alias of the join relationship
     *
     * @return string
     */
    private function findRootAlias($alias, $parentAlias)
    {
        $rootAlias = null;
        if (in_array($parentAlias, $this->getRootAliases(), true)) {
            $rootAlias = $parentAlias;
        } elseif (isset($this->joinRootAliases[$parentAlias])) {
            $rootAlias = $this->joinRootAliases[$parentAlias];
        } else {
            // Should never happen with correct joining order. Might be
            // thoughtful to throw exception instead.
            $rootAlias = $this->getRootAlias();
        }
        $this->joinRootAliases[$alias] = $rootAlias;
        return $rootAlias;
    }
    
    
    public function getRootAlias()
    {
        $aliases = $this->getRootAliases();
        if (!isset($aliases[0])) {
            throw new \RuntimeException('No alias was set before invoking getRootAlias().');
        }
        return $aliases[0];
    }
    
    
    public function getRootAliases()
    {
        $aliases = [];
        foreach ($this->parts['from'] as &$fromClause) {
            if (is_string($fromClause)) {
                $spacePos = strrpos($fromClause, ' ');
                $from = substr($fromClause, 0, $spacePos);
                $alias = substr($fromClause, $spacePos + 1);
                $fromClause = new From($from, $alias);
            }
            $aliases[] = $fromClause->getAlias();
        }
        return $aliases;
    }
    
    
}