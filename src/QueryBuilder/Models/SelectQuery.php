<?php
/**
 * @author Sam Street
 */

namespace QueryBuilder\Models;

use QueryBuilder\Interfaces\QueryBuilderInterface;

class SelectQuery implements QueryBuilderInterface
{
    /**
     * @var string
     */
    private $selectPart;
    
    private $parts;
    
    /**
     * SelectQuery constructor.
     * @param array $select
     */
    function __construct(array $select)
    {
        $this->parts = $select;
    }
    
    /**
     * @param array $parts
     * @return string
     */
    public function build()
    {
        $string = "SELECT ";
        foreach ($this->parts as $part) {
            if( count($part->getParts()) > 1 ){
                foreach ($part->getParts() as $inner) {
                    $string .= $inner . $part->getConnector();
                }
            } else {
                $string .= $part->getParts()[0] . " ";
            }
            
        }
        
        $this->selectPart = trim($string, ', ') . " ";
        return $this->selectPart;
        
    }
    
}
