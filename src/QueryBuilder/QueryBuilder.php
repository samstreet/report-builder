<?php

namespace QueryBuilder;

use QueryBuilder\Interfaces\ReportInterface;
use QueryBuilder\Models\QueryBuilder as Generator;

/*
Plugin Name: Report Builder
Description: A plugin for creating an reports
Version: 0.0.1
Author: Sam Street
*/

/**
 * Class QueryBuilder
 *
 * @package QueryBuilder
 */
class QueryBuilder implements ReportInterface
{
    
    /**
     * @var \QueryBuilder\Models\QueryBuilder
     */
    private $builder;
    
    /**
     * QueryBuilder constructor.
     *
     * @param \QueryBuilder\Models\QueryBuilder $builder
     */
    public function __construct(Generator $builder)
    {
        
        $this->builder = $builder;
        
    }
    
    /**
     * @return string
     */
    public function make()
    {
        return $this->builder->make();
    }
    
    public function select($select = null)
    {
        return $this->builder->select($select);
    }
    
    public function addSelect($select = null)
    {
        // TODO: Implement addSelect() method.
    }
    
    public function delete($delete = null, $alias = null)
    {
        // TODO: Implement delete() method.
    }
    
    public function update($update = null, $alias = null)
    {
        // TODO: Implement update() method.
    }
    
    public function set($key, $value)
    {
        // TODO: Implement set() method.
    }
    
    public function from($from, $alias, $indexBy = null)
    {
        return $this->builder->from($from, $alias, $indexBy);
    }
    
    public function join($join, $alias, $conditionType = null, $condition = null, $indexBy = null)
    {
        return $this->builder->innerJoin($join, $alias, $conditionType, $condition, $indexBy);
    }
    
    public function innerJoin($join, $alias, $conditionType = null, $condition = null, $indexBy = null)
    {
        // TODO: Implement innerJoin() method.
    }
    
    public function leftJoin($join, $alias, $conditionType = null, $condition = null, $indexBy = null)
    {
        return $this->builder->leftJoin( $join, $alias, $conditionType, $condition, $indexBy);
    }
    
    public function where($where)
    {
        // TODO: Implement where() method.
    }
    
    public function andWhere($where)
    {
        // TODO: Implement andWhere() method.
    }
    
    public function orWhere($where)
    {
        // TODO: Implement orWhere() method.
    }
    
    public function groupBy($groupBy)
    {
        // TODO: Implement groupBy() method.
    }
    
    public function addGroupBy($groupBy)
    {
        // TODO: Implement addGroupBy() method.
    }
    
    public function having($having)
    {
        // TODO: Implement having() method.
    }
    
    public function andHaving($having)
    {
        // TODO: Implement andHaving() method.
    }
    
    public function orHaving($having)
    {
        // TODO: Implement orHaving() method.
    }
    
    public function orderBy($sort, $order = null)
    {
        // TODO: Implement orderBy() method.
    }
    
    public function addOrderBy($sort, $order = null)
    {
        // TODO: Implement addOrderBy() method.
    }
    
    
}
