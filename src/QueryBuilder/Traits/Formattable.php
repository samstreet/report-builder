<?php

namespace QueryBuilder\Traits;

/**
 * @author Sam Street
 */
trait Formattable {
	
	public function format( $query ) {
		$keywords = array(
			"select",
			"from",
			"where",
			"order by",
			"group by",
			"insert into",
			"update",
			"left join",
			','
		);
		foreach ( $keywords as $keyword ) {
			if ( preg_match( "/($keyword *)/i", $query, $matches ) ) {
				$query = str_replace( $matches[1], "\n" . strtoupper( $matches[1] ) . "", $query );
			}
		}
		
		var_dump( $query );
	}

}